This is Very Simple # Text file splitter #

argument explain(* is must be input. others are optional)

    *first argument :: original file's **absolute path**
    *second argument :: separated files's destination directory **absolute path**
    third argument :: maximum lines of each destination files during file split