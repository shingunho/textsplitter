package org.exbeta.text.splitter;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class App {

	static int MAX_LINES = 10000;
	static String savePath;
	static String originalName;
	static String extention;

	public static void helpPrint() {
		System.out.println("------- HOW TO USE");
		System.out
				.println("    * is must be input. others are optional -------------------\n");
		System.out
				.println("        *first argument :: original file's absolute path");
		System.out
				.println("        *second argument :: separated files's destination directory absolute path");
		System.out
				.println("         third argument :: maximum lines of each destination files during file split");
	}

	public static void main(String[] args) throws IOException {

		if (args[0].toLowerCase().equals("help") || args.length > 3) {
			App.helpPrint();
		}

		String filePath = args[0];
		savePath = args[1];

		if (args.length == 3) {
			App.MAX_LINES = Integer.parseInt(args[2]);
		}

		File origin = new File(filePath);
		originalName = origin.getName().substring(0,
				origin.getName().lastIndexOf("."));
		extention = origin.getName().substring(
				origin.getName().lastIndexOf(".") + 1);

		BufferedReader reader = new BufferedReader(new FileReader(filePath));

		App app = new App();
		app.fileWrite(reader, filePath);

		System.out.println("COMPLETE!");
	}

	static int FILENUMBER = 0;
	static long OFFSET = 0;

	protected void fileWrite(BufferedReader reader, String filePath) {
		BufferedWriter outStream = null;
		try {
			outStream = new BufferedWriter(new FileWriter(savePath + "/"
					+ originalName + "_" + FILENUMBER + "." + extention));
			String read;

			if (FILENUMBER != 0) {
				for (long skip = 0l; skip < OFFSET; skip++) {
					reader.readLine();
				}
			}

			while ((read = reader.readLine()) != null) {
				System.out.println("OFFSET : " + OFFSET + ", CONTENTS : "
						+ read.replaceAll("\r\n", ""));
				if(read.toLowerCase().contains("to_date('2013")){
					outStream.write(read);
					outStream.newLine();	
				}
				OFFSET++;

				if (OFFSET % App.MAX_LINES == 0) {
					outStream.close();
					FILENUMBER++;
					fileWrite(reader, filePath);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				outStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
